using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class APExtensions
{
    public static Vector3 GetGroundHitPoint(this Transform t)
    {
        RaycastHit hit;
        if (Physics.Raycast(new Ray(new Vector3(t.position.x, t.position.y + 500, t.position.z), Vector3.down),
            out hit, 10000f, 1 << ConstantManager.GROUND_LAYER))
        {
            return hit.point;
        }
        return t.position;
    }

    public static void ActiveChildByIndex(this Transform _t, int childIndex)
    {
        for (int i = 0; i < _t.childCount; i++)
        {
            _t.GetChild(i).gameObject.SetActive(i == childIndex);
        }
    }

    public static float DistanceFrom(this Transform _transform, Transform comparingTransform)
    {
        return DistanceFrom(_transform.position, comparingTransform.position);
    }

    public static float DistanceFrom(this Vector3 selfPosition, Vector3 comparingPosition)
    {
        return Vector3.Distance(selfPosition, comparingPosition);
    }

    public static Vector3 ModifyThisVector(this Vector3 value, float x, float y, float z)
    {
        return new Vector3(value.x + x, value.y + y, value.z + z);
    }

    public static Vector3 ModifyThisVector(this Vector3 value, Vector3 vector)
    {
        return value.ModifyThisVector(vector.x, vector.y, vector.z);
    }

    public static Transform GetClosestTransform(this Transform t, List<Transform> list)
    {
        List<Transform> transforms = list.OrderBy(i => Vector3.Distance(t.position, i.position)).ToList();
        return transforms.Count > 0? transforms[0] : null;
    }
}
